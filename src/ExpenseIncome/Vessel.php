<?php
namespace App\ExpenseIncome;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Vessel extends  DB{

    private $modifiedDate, $categoryid,$shipid;
    private $vesselName;
    private $importerName;
    private $bankId;
    private $lcNo;
    private $lcDate;
    private $ldt;
    private $dollarPrice;
    private $dollarRate;
    private $wastage;
    private $wastageValue;
    private $remarks;

    public function setData($postData){

        if(array_key_exists('modifiedDate',$postData)){
            $this->modifiedDate = $postData['modifiedDate'];
        }
        if(array_key_exists('vesselName',$postData)){
            $this->vesselName = $postData['vesselName'];
        }
        if(array_key_exists('shipid',$postData)){
            $this->shipid = $postData['shipid'];
        }
        if(array_key_exists('vesselid',$postData)){
            $this->shipid = $postData['vesselid'];
        }
        if(array_key_exists('importerName',$postData)){
            $this->importerName = $postData['importerName'];
        }
         if(array_key_exists('bankId',$postData)){
            $this->bankId = $postData['bankId'];
        }
         if(array_key_exists('lcNo',$postData)){
            $this->lcNo = $postData['lcNo'];
        }
        if(array_key_exists('lcNo',$postData)){
            $this->lcNo = $postData['lcNo'];
        }
         if(array_key_exists('lcDate',$postData)){
            $this->lcDate = $postData['lcDate'];
        }
        if(array_key_exists('ldt',$postData)){
            $this->ldt = $postData['ldt'];
        }
        if(array_key_exists('dollarPrice',$postData)){
            $this->dollarPrice = $postData['dollarPrice'];
        }
        if(array_key_exists('dollarRate',$postData)){
            $this->dollarRate = $postData['dollarRate'];
        }
        if(array_key_exists('wastage',$postData)){
            $this->wastage= $postData['wastage'];
        }
        if(array_key_exists('wastageValue',$postData)){
            $this->wastageValue= $postData['wastageValue'];
        }
        if(array_key_exists('remarks',$postData)){
            $this->remarks= $postData['remarks'];
        }

    }
    public function store(){
        $this->categoryid=1;
        $arrData = array($this->modifiedDate,$this->vesselName,$this->categoryid,$this->importerName,$this->bankId,$this->lcNo,$this->lcDate,$this->ldt,$this->dollarPrice,$this->dollarRate,$this->wastage,$this->wastageValue,$this->remarks);
        $sql = "INSERT into ship(created,vesselname,categoryid,importername,bankid,lcno,lcdate,ldt,dollarprice,dollarrate,wastage,wastagevalue,remarks) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData);
        if($result){
            $arrData = array($this->modifiedDate,$this->vesselName,$this->categoryid,$this->importerName,$this->bankId,$this->lcNo,$this->lcDate,$this->ldt,$this->dollarPrice,$this->dollarRate,$this->wastage,$this->wastageValue,$this->remarks);
            $sql = "INSERT into products(created,product_name,categoryid,importername,bankid,lcno,lcdate,ldt,dollarprice,dollarrate,wastage,wastagevalue,remarks) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $STH = $this->DBH->prepare($sql);
            $queryResult =$STH->execute($arrData);
            if($queryResult)
                Message::message("Success! Data Has Been Inserted Successfully :)");
        }
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('index.php');
    }
    public function viewallvessel(){

        $sql="SELECT * from ship left join bank on ship.bankid=bank.id where ship.soft_deleted='No' ORDER BY vesselname ASC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function viewVessel(){

        $sql="SELECT * from ship  WHERE id='$this->shipid'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function delete(){

        $sql = "UPDATE  ship SET soft_deleted='Yes' WHERE id=".$this->shipid;

        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Soft Deleted  :( ");

        Utility::redirect('index.php');
    }
    public function update(){
       // var_dump($_POST); die();

        $arrData = array($this->modifiedDate,$this->vesselName,$this->categoryid,$this->importerName,$this->bankId,$this->lcNo,$this->lcDate,$this->ldt,$this->dollarPrice,$this->dollarRate,$this->wastage,$this->wastageValue,$this->remarks);
        $sql = "UPDATE ship SET created=?,vesselname=?,categoryid=?,importername=?,bankid=?,lcno=?,lcdate=?,ldt=?,dollarprice=?,dollarrate=?,wastage=?,wastagevalue=?,remarks=? WHERE id='$this->shipid'";
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData);
        if($result)
            Message::message("Success! Vessel Name Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated :( ");
        Utility::redirect('view.php?id=vessel');
    }


}