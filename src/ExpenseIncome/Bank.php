<?php
namespace App\ExpenseIncome;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Bank extends  DB{

    private $bankname, $accountname, $accountnumber, $branch, $address,$bankid;

    public function setData($postData){

        if(array_key_exists('modifiedDate',$postData)){
            $this->modified_Date = $postData['modifiedDate'];
        }
        if(array_key_exists('bankname',$postData)){
            $this->bankname = $postData['bankname'];
        }
        if(array_key_exists('accountname',$postData)){
            $this->accountname = $postData['accountname'];
        }
         if(array_key_exists('accountnumber',$postData)){
            $this->accountnumber = $postData['accountnumber'];
        }
         if(array_key_exists('branch',$postData)){
            $this->branch = $postData['branch'];
        }
        if(array_key_exists('address',$postData)){
            $this->address = $postData['address'];
        }
        if(array_key_exists('bankId',$postData)){
            $this->bankid = $postData['bankId'];
        }
    }
    public function store(){

        $arrData = array($this->bankname,$this->accountname,$this->accountnumber,$this->branch,$this->address,$this->modified_Date);
        $sql = "INSERT into bank(bankname,accountname,accountnumber,branch,address,created) VALUES(?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData);
        if($result)
            Message::message("Success! New Bank Account Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('index.php');
    }

    public function viewallbank(){

        $sql="SELECT * from bank where soft_deleted='No' ORDER BY bankname ASC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function viewBank(){

        $sql="SELECT * from bank  WHERE id='$this->bankid'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function delete(){

        $sql = "UPDATE  bank SET soft_deleted='Yes' WHERE id=".$this->bankid;

        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Soft Deleted  :( ");

        Utility::redirect('index.php');
    }
    public function update(){

        $arrData = array($this->bankname,$this->accountname,$this->accountnumber,$this->branch,$this->address);

        $sql = "UPDATE bank SET bankname=?,accountname=?, accountnumber=?,branch=?,address=? WHERE id='$this->bankid'";
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData);
        if($result)
            Message::message("Success! Bank Name Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated :( ");
        Utility::redirect('index.php');
    }


}