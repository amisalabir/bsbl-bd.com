<?php
include_once('../vendor/autoload.php');
include 'namespace.php';
include('header.php');
?>


	<div class="content">
		<div class="container ctn">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10 main">
					<form class="multipleTranscation">
						<div class="control">
							<div class="row">
								<div class="col-md-7 text-right">
									<h2>BSBL</h2>
									<strong>Trading Account (Year Ended)</strong>
								</div>
								<div class="col-md-5">
									<p class="nick text-right">Date: 03-12-2018</p>
								</div>
							</div>
						</div><hr/>
						<div class="row">
							<div class="col-md-6">
								<h3>To</h3>
								<table class="table table-stripped">
									<thead>
										<tr>
											<th>Summery</th>
											<th></th>
											<th>Balance</th>
										</tr>
									</thead>
									<tr>
										<td>Opening Balance</td>
										<td>:</td>
										<td>2000</td>
									</tr>
									<tr>
										<td>Cutting</td>
										<td>:</td>
										<td>2000</td>
									</tr>
									<tr>
										<td>Wages</td>
										<td>:</td>
										<td>2000</td>
									</tr>
									<tr>
										<td class="text-right"><strong>Total</strong></td>
										<td></td>
										<td><strong>6000</strong></td>
									</tr>

								</table>
							</div>
							<div class="col-md-6">
								<h3>By</h3>
								<table class="table table-stripped">
									<thead>
										<tr>
											<th>Summery</th>
											<th></th>
											<th>Balance</th>
										</tr>
									</thead>
									<tr>
										<td>Opening Balance</td>
										<td>:</td>
										<td>2000</td>
									</tr>
									<tr>
										<td>Cutting</td>
										<td>:</td>
										<td>2000</td>
									</tr>
									<tr>
										<td>Wages</td>
										<td>:</td>
										<td>2000</td>
									</tr>
									<tr>
										<td class="text-right"><strong>Total</strong></td>
										<td></td>
										<td><strong>6000</strong></td>
									</tr>

								</table>
							</div>
						</div><hr/>
						<div class="row">
							<div class="text-center">
								<strong>Profit Sheet</strong>
								<p>For the year of ended on 03-12-2018</p>
							</div>
							<div class="col-md-6">
								<h3>To</h3>
								<table class="table table-stripped">
									<thead>
										<tr>
											<th>Summery</th>
											<th></th>
											<th>Balance</th>
										</tr>
									</thead>
									<tr>
										<td>Opening Balance</td>
										<td>:</td>
										<td>2000</td>
									</tr>
									<tr>
										<td>Cutting</td>
										<td>:</td>
										<td>2000</td>
									</tr>
									<tr>
										<td>Wages</td>
										<td>:</td>
										<td>2000</td>
									</tr>
									<tr>
										<td class="text-right"><strong>Total</strong></td>
										<td></td>
										<td><strong>6000</strong></td>
									</tr>

								</table>
							</div>
							<div class="col-md-6">
								<h3>By</h3>
								<table class="table table-stripped">
									<thead>
										<tr>
											<th>Summery</th>
											<th></th>
											<th>Balance</th>
										</tr>
									</thead>
									<tr>
										<td>Opening Balance</td>
										<td>:</td>
										<td>2000</td>
									</tr>
									<tr>
										<td>Cutting</td>
										<td>:</td>
										<td>2000</td>
									</tr>
									<tr>
										<td>Wages</td>
										<td>:</td>
										<td>2000</td>
									</tr>
									<tr>
										<td class="text-right"><strong>Total</strong></td>
										<td></td>
										<td><strong>6000</strong></td>
									</tr>

								</table>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="text-center">
								<strong>Balance Sheet</strong>
								<p>As on 30-12-2018</p>
							</div>
							<div class="col-md-6">
								<h3>To</h3>
								<table class="table table-stripped">
									<thead>
										<tr>
											<th>Summery</th>
											<th></th>
											<th>Balance</th>
										</tr>
									</thead>
									<tr>
										<td>Opening Balance</td>
										<td>:</td>
										<td>2000</td>
									</tr>
									<tr>
										<td>Cutting</td>
										<td>:</td>
										<td>2000</td>
									</tr>
									<tr>
										<td>Wages</td>
										<td>:</td>
										<td>2000</td>
									</tr>
									<tr>
										<td class="text-right"><strong>Total</strong></td>
										<td></td>
										<td><strong>6000</strong></td>
									</tr>

								</table>
							</div>
							<div class="col-md-6">
								<h3>By</h3>
								<table class="table table-stripped">
									<thead>
										<tr>
											<th>Summery</th>
											<th></th>
											<th>Balance</th>
										</tr>
									</thead>
									<tr>
										<td>Opening Balance</td>
										<td>:</td>
										<td>2000</td>
									</tr>
									<tr>
										<td>Cutting</td>
										<td>:</td>
										<td>2000</td>
									</tr>
									<tr>
										<td>Wages</td>
										<td>:</td>
										<td>2000</td>
									</tr>
									<tr>
										<td class="text-right"><strong>Total</strong></td>
										<td></td>
										<td><strong>6000</strong></td>
									</tr>

								</table>
							</div>
						</div>

					</form>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</div>



<?php
include ('footer.php');
include ('footer_script.php');
?>
