<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('index.php');
    return;
}

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {

    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('index.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
################################ End of Session time calculation ##############################
$objHead = new \App\ExpenseIncome\Head();
$objParty = new \App\ExpenseIncome\Party();
$objBank = new \App\ExpenseIncome\Bank();
$objVessel = new \App\ExpenseIncome\Vessel();
$msg = Message::getMessage();
############################## Message code ended #############################################
############################### Delete code started ###########################################
if(isset($_GET['accheadId']) && isset($_GET['YesButton'])){$objHead->setData($_GET);$objHead->delete();}
if(isset($_GET['partyid']) && isset($_GET['YesButton'])){$objParty->setData($_GET);$objParty->delete();}
if(isset($_GET['bankid']) && isset($_GET['YesButton'])){$objBank->setData($_GET);$objBank->delete();}
if(isset($_GET['shipid']) && isset($_GET['YesButton'])){$objVessel->setData($_GET);$objVessel->delete();}
############################### Delete code ended ###########################################
include_once ('header.php');
include_once('printscript.php');
?>
<div align="center" class="content">
    <div align="center" class="container ctn">
        <div align="center" class="container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'> $msg </div> </div>"; ?>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
        <form action="trashmultiple.php" method="post" id="multiple">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="navbar-header">
                        <button style="background-color: #8aa6c1;" type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbarTwo" aria-expanded="false" aria-controls="navbarTwo">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <?php
                    $userButton= "<div id=\"navbarTwo\" class=\"navbar-collapse collapse\">
                        <ul class=\"nav navbar-nav navbar-right\">
                            <button type=\"button\"   id=\"btnPrint\" value=\"Print Div Contents\" class=\"btn btn-primary active \">Print</button>
                            <a href=\"pdf.php\" class=\"btn btn-primary \"  >Download as PDF</a>
                            <a href=\"xl.php\" class=\"btn btn-primary\" \" >Download as XL</a>
                            <a href=\"email.php?list=1\" class=\"btn btn-primary\" role=\"button\" >Email</a>
                            ";
                    $adminButton="<button type=\"button\" class=\"btn btn-danger\" id=\"delete\">Delete  Selected</button>
                            <button type=\"submit\" class=\"btn btn-warning\">Trash Selected</button>
                            <a href=\"trashed . php ? Page = 1\"   class=\"btn btn-info\" > View Trashed List</a>
                            </ul></div>
                        ";
                    if($singleUser->role=='admin'){
                        echo $userButton.$adminButton;
                    } else{echo $userButton;}
                    ?>
                </div>
                <div class="col-md-1"></div>
            </div>
            <span><br><br> </span>
            <div class="container">
                <div id="row">
                    <div class="col-md-3"></div>
                    <div class="col-sm-6">
                    <style><?php include ('../resource/css/printsetup.css');   ?>   </style>
                    <h4 class="text-danger">Do you want to delete this record ?</h4>
                    <hr style="background-color: #0f0f0f;">
                    <h4 style="background-color: ;" >
                    <?php
                    if(isset($_GET['accheadId'])){include 'accounthead/deletehead.php';}
                    if(isset($_GET['partyid'])){include 'party/deleteparty.php';}
                    if(isset($_GET['bankid'])){include 'bank/deletebank.php';}
                    if(isset($_GET['vesselid'])){

                        include 'vessel/deletevessel.php';
                    }
                    ?>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
include ('footer.php');
include ('footer_script.php');
?>
