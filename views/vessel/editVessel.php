
<div class="content">
    <div class="container ctn">
        <div class="row">  <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success' id='message'> $msg</div> </div>"; ?> </div>
        <div class="container"><br></div>
        <div class="row">
    <script type="text/javascript">
        function ChequeField(val){
            var element=document.getElementById('activeChequeField');
            if(val=='CASH CHEQUE'){
                element.style.display='block';
            }
            else{
                element.style.display='none';
            }
        }
    </script>
    <form class="form-group" name="vesselEntry" action="update.php" method="post">
        <input hidden name="updateVessel" type="text" value="updateVessel">
        <input hidden name="shipid" type="text" value="<?php echo $objVesselToArray['0']['id']; ?>">
        <input name="modifiedDate"  type="text" hidden  value="<?php echo date('Y-m-d');?>">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="transactionType">VESSEL NAME :</label> </div>
                    <div class="col-sm-4 text-left">
                        <input class="form-control " id="" name="vesselName" value="<?php echo $objVesselToArray['0']['vesselname']; ?>" required type="text">
                    </div>
                        <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedClient">IMPOTER NAME :</label> </div>
                    <div class="col-sm-4 text-left ">
                        <input class="form-control " id="" name="importerName" value="<?php echo $objVesselToArray['0']['importername']; ?>" required type="text">
                        </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedClient">BANK NAME :</label> </div>
                    <div class="col-sm-6 text-left ">
                        <select  name="bankId"  class="form-control text-uppercase ">
                            <?php
                            foreach ($bankNme as $singlBank){
                                echo  "<option class='text-uppercase' value='$singlBank->id'> $singlBank->bankname ($singlBank->accountname) </option>";}
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="lcNo"> LC NO :</label></div>
                    <div class="col-sm-2 text-left">
                        <input class="form-control" name="lcNo" value="<?php echo $objVesselToArray['0']['lcno']; ?>" type="text" required>
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="lcDate">  LC DATE :</label></div>
                    <div class="col-sm-2 text-left">
                        <input class="form-control selectDate" name="lcDate" value="<?php echo $objVesselToArray['0']['lcdate']; ?>" required  type="text">
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="ldt">LDT :</label></div>
                    <div class="col-sm-2 text-left">
                        <input class="form-control " id="" name="ldt" value="<?php echo $objVesselToArray['0']['ldt']; ?>" required type="text">
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="dollarPrice">DOLLAR PRICE :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="dollarPrice" value="<?php echo $objVesselToArray['0']['dollarprice']; ?>" type="text" required>
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="dollarRate">DOLLAR RATE :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="dollarRate" value="<?php echo $objVesselToArray['0']['dollarrate']; ?>"  type="text" required>
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="txtAmount"> WASTAGE :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="wastage"  id="wastage" value="<?php echo $objVesselToArray['0']['wastage']; ?>"required type="text">
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="wastageValue"> WASTAGE VALUE  :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="wastageValue"  id="wastageValue" value="<?php echo $objVesselToArray['0']['wastagevalue']; ?>" required type="number">
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="row">

                    <div class="col-sm-4 text-right form-group"><label for="remarks"> REMARKS :</label></div>
                    <div class="col-sm-5 text-left">
                        <textarea class="form-control"  name="remarks" rows="2" value="<?php echo $objVesselToArray['0']['remarks']; ?>" cols="20"  ></textarea>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </div>
                <div class="row">
                    <div class="col-sm-5"></div>
                    <div class="col-sm-3 text-right form-group">
                       <!-- <button  type="submit" class="btn btn-primary form-control">Submit</button>-->
                        <input type="submit" class="btn-primary form-control" value="Submit">
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="col-sm-1"></div>
</div>
    </form>

        </div>
    </div>
</div>
