<?php

?>
<table >
    <thead>
    <tr>
     <td colspan="3" align="center" >
     <font  style="text-align: center;  text-transform:uppercase; font-weight: bold; font-size:25px;">Bhatiyari Ship Breakers Ltd.</font> <br>
     <font style="font-size:14px">Bhatiyari, Sitakunda, Chittagong.</font><br>
     </td>
      </tr>
      <tr><td ><b><?php  echo "List of Vessels"; ?></b></td> <td></td> <td style="text-align: right; font-size:12;"><?php echo "Print Date: ";  echo date('Y-m-d'); ?> </td></tr>
      </thead>
    <tr> <td colspan="3">
    <!-- Inner Table -->
     <div class="row" align="center">
      <div id="reporttable" class="col-sm-12 text-center" align="center" >
     <table width="auto"   class="" >
       <thead>
        <tr style="background-color:#F2F2F2;">
         <th class="text-center">SL</th>
         <th class="text-center">Vessel Name</th>
            <th class="text-center">Bank</th>
            <th class="text-center">LC No</th>
            <th class="text-center">LC Date</th>
            <th class="text-center">LDT (MT)</th>
            <th class="text-center">Dollar Price</th>
            <th class="text-center">Dollar Rate</th>
         <th class="text-center">Actions</th>
        </tr>
       </thead>
       <?php
       foreach($allVessel as $oneData){
        if($serial%2) $bgColor = "AZURE";
        else $bgColor = "#ffffff";
             echo "
                  <tr  style='background-color: $bgColor'>
                     <td style='text-align: center;'>".$serial."</td>
                     <td class='text-left'> $oneData->vesselname ($oneData->id) </td>
                     <td class='text-left'> $oneData->bankname</td>
                     <td class='text-left'> $oneData->lcno</td>
                     <td class='text-left'> $oneData->lcdate</td>
                     <td class='text-left'> $oneData->ldt</td>
                     <td class='text-left'> $oneData->dollarprice</td>
                     <td class='text-left'> $oneData->dollarprrate</td>
                     <td class='text-right'>";
                      if($singleUser->role=='admin') {
                          echo "
                       <a role='menuitem' tabindex=-1' href='edit.php?shipid&vesselid=$oneData->id'>Edit</a> |
                       <a role='menuitem' tabindex=-1' href='delete.php?vesselid=$oneData->id'>Delete</a>";
                      }

                    echo"</td></tr>";
              $serial++; }
?>
      </table>
          <br>
      </div>
      </div>
    </td>
   </tr>
</table>