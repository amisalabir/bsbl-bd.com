<?php 
include('header.php');
?>
	<div class="content">
		<div class="container ctn">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10 main">
					<form class="multipleTranscation">
						<div class="control">
							<div class="row">
								<div class="col-md-6">
									<a href="#" class="btn btn-secondary">EDIT</a>
									<a href="#" class="btn btn-secondary">Refresh</a>
								</div>
								<div class="col-md-6">
									<p class="nick text-right">Job Expenses</p>
								</div>
							</div>
						</div><hr/>
						
						
						
						<table class=" table table-responsive one">
							<tr>
								<td>
								<div class="col-auto form-inline">
								Expense Date :
									<input type="date" name="ExpDate" class="form-control" required>&nbsp;&nbsp;&nbsp;
								Invoice No :
									<input type="number" name="invNo" class="form-control" required>
								</div>
								</td>
							</tr>
							<tr>
								<td>
								<div class="col-auto form-inline">
								Job No & Year :
									<input type="text" name="jobNo" class="form-control" required>
									<input style="width:150px;" type="text" name="jobNo2" class="form-control" required>
									<input style="width:150px;" type="text" name="jobYear" placeholder="Job Year" class="form-control" required>
								</div>
								</td>
							</tr>
							<tr>
								<td>
								<div class="col-auto form-inline">
								Expensed By :
									<input style="width:150px;" type="text" name="ExpBy" class="form-control" required>
									<input style="width:150px;" type="text" name="ExpBy2" class="form-control" required>&nbsp;
									Remarks :
									<input style="width:150px;" type="text" name="remarks" class="form-control" required>
								</div>
								</td>
							</tr>
						</table><br/>
						<table class="table table-responsive table-bordered tbtwo">
							<thead class="thead-light">
							<tr style="background-color:#4A3C8C;color:#FFFFFF;">
								<th>Serial</th>
								<th>Particulars</th>
								<th>Code</th>
								<th>Expense Amount</th>
								<th>Remarks</th>
								<th></th>
							</tr>
							</thead>
							<tr>
								<td>1</td>
								<td><input type="text" class="form-control" name="Particulars" required></td>
								<td><input type="text" class="form-control" name="Code" required></td>
								<td><input type="number" class="form-control" name="expAmount" required></td>
								<td><input type="text" class="form-control" name="remarks" required></td>
								<td style="width:100px;">
									<a href="#"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>&nbsp;
									<a href="#"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></a>&nbsp;
									<a href="#"><span class="glyphicon glyphicon-print" aria-hidden="true"></span></a>
								</td>
							</tr>
							<tr>
								<td colspan="6">
								<div class="col-auto form-inline"><span style="padding-left:450px;">total :</span><input style="width:100px;" type="number" name="total" class="form-control"></div>
								</td>
							</tr>
						</table>
						<br/><div align="right"><input type="submit" class="btn btn-primary" name="submit" value="Submit"></div>
					</form>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</div>
 <?php 
include('footer.php');
?> 
 