<?php 
include('header.php');
?>
	<div class="content">
		<div class="container ctn">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6 main">
					<form class="signleTranscation">
						<div class="control">
							<div class="row">
								<div class="col-md-6">
									<a href="#" class="btn btn-secondary">EDIT</a>
									<a href="#" class="btn btn-secondary">Refresh</a>
								</div>
								<div class="col-md-6">
									<p class="nick text-right">Job Register - Station Wise</p>
								</div>
							</div>
						</div>
						<table class="table table-responsive" border="0">
							<tr>
								<td>Register ID</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="regId" required></td>
							</tr>
							<tr>
								<td>Job Year</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="jobYear" required></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="submit" class="btn btn-primary" name="Search" value="View Report"></td>
							</tr>
						</table>
					</form>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</div>
 <?php 
include('footer.php');
?> 
 